# double-linkedlist

CONSIDERACIONES GENERALES

- Implementación de lista doblemente enlazada partiendo de la interfaz List de java.util.List
- Como herramienta de construcción se usa Maven, Java 8 y codificación UTF-8
- Empaquetado como bundle para publicación en contenerdor OSGi
- Basado en la implementación de LinkedList para el nombrado de atributos (first, last, size)
- Se ha intentado usar en la medida de lo posible nuevas características de Java 8 como la API Stream y funciones lambda
- No se han implementado los métodos para usar iteradores (y alguno), interesante para los recorridos decrementales (por falta de tiempo...)
- Uso de plugin Jacoco de Maven para indicar cobertura de código
- Comentarios usando solamente caracteres ASCII (se agradece para conversiones UTF-8/ISO-8859-1)
- La implementación de la lista no soporta accesos concurrentes (se soluciona encapsulándola en Collections.synchronizedList)
- La adición de elementos a la lista no crea una copia, usa la misma referencia
