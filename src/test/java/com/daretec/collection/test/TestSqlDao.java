/**
 * 
 */
package com.daretec.collection.test;

import java.sql.SQLException;
import java.util.Map;

/**
 * @author abelon
 *
 */
public interface TestSqlDao {
	
	// retornamos una referencia a Map, no una implementacion
	Map<Long, Long> getMaxUserOrderId(long idTienda) throws SQLException; // mala practiva lanzar Exception
	
	void copyUserOrders(long idUserOri, long idUserDes) throws SQLException;
	
	void getUserMaxOrder(long idTienda, long userId, long orderId, String name, String address) throws SQLException;

}
