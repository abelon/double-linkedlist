package com.daretec.collection.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;

	/**
	 * Mejorar cada uno de los métodos a nivel SQL y código cuando sea necesario
	 * Razonar cada una de las mejoras que se han implementado
	 * No es necesario que el código implementado funcione 
	 */

	/**
	 * Implementamos clase a partir de una interface
	 * En la medida de lo posible, las clases deben de depende de interfaces y no de implementaciones concretas
	 * 
	 * @author abelon
	 *
	 */
	public class TestSqlDaoImpl implements TestSqlDao {

		// usamos el patron singleton: perdemos rendimiento pero ganamos escalabilidad
		private static TestSqlDao instance;
		
		// ¡¡ERROR!! No usar atributos de clase cuando puede haber acceso concurrentes
		//private Hashtable<Long, Long> maxOrderUser;
		
		private TestSqlDaoImpl() {}

		// este metodo debe ser publico para poder obtener la instancia
		public static TestSqlDao getInstance() {
			// doble check para evitar crear multiples instancias en accesos concurrentes
			if(Objects.isNull(instance)) {
				synchronized(TestSqlDaoImpl.class) {
					if(Objects.isNull(instance)) {
						instance = new TestSqlDaoImpl();
					}
				}
			}
			return instance;
		}

		/**
		 * Obtiene el ID del último pedido para cada usuario
		 * 
		 * @throws SQLException 
		 */
		public Map<Long, Long> getMaxUserOrderId(final long idTienda) throws SQLException {

			//final String query = String.format("SELECT ID_PEDIDO, ID_USUARIO FROM PEDIDOS WHERE ID_TIENDA = %s", idTienda);
			final Connection connection = getConnection();
			// optimizacion para solo consultas
			connection.setReadOnly(true);
			// uso de ? para el paso de atributos facilitando la labor al SGBD
			// agrupamos en la propia sentencia SQL para que nos salga directamente el maximo pedido de cada usuario
			final PreparedStatement stmt = connection.prepareStatement("SELECT MAX(ID_PEDIDO), ID_USUARIO FROM PEDIDOS WHERE ID_TIENDA = ? GROUP BY ID_USUARIO");
			stmt.setLong(1, idTienda);
			final ResultSet rs = stmt.executeQuery();
			// definimos objeto de ambito local
			final Map<Long, Long> maxOrderUser = new Hashtable<Long, Long>();
			// guardamos directamente los resultados en el map
			long idPedido, idUsuario = 0;
			while (rs.next()) {

				idPedido = rs.getInt("ID_PEDIDO");
				idUsuario = rs.getInt("ID_USUARIO");
				
				//if (!maxOrderUser.containsKey(idUsuario)) {

					maxOrderUser.put(idUsuario, idPedido);

				//} else if (maxOrderUser.get(idUsuario) < idPedido) {

				//	maxOrderUser.put(idUsuario, idPedido);
				//}
			}

			return maxOrderUser;
		}

		/**
		 * Copia todos los pedidos de un usuario a otro
		 */
		public void copyUserOrders(final long idUserOri, final long idUserDes) throws SQLException {

			//String query = String.format("SELECT FECHA, TOTAL, SUBTOTAL, DIRECCION FROM PEDIDOS WHERE ID_USUARIO = %s", idUserOri);
			final Connection connection = getConnection();
			//PreparedStatement stmt = connection.prepareStatement(query);
			//ResultSet rs = stmt.executeQuery();

			//while (rs.next()) {

			// ejecutamos la query en una unica sentencia, haciendo un insert en bloque cuyos datos los recoge de una select
			final PreparedStatement stmt = connection.prepareStatement("INSERT INTO PEDIDOS (FECHA, TOTAL, SUBTOTAL, DIRECCION, ID_USUARIO) SELECT FECHA, TOTAL, SUBTOTAL, DIRECCION, " + idUserDes + " AS ID_USUARIO FROM PEDIDOS WHERE ID_USUARIO = ?");
			stmt.setLong(1, idUserOri);
			stmt.executeUpdate();
			//rs.getTimestamp("FECHA"),
			//rs.getDouble("TOTAL"),
			//rs.getDouble("SUBTOTAL"),
			//rs.getString("DIRECCION"));

			//Connection connection2 = getConnection();
			//connection2.setAutoCommit(false);
			//PreparedStatement stmt2 = connection2.prepareStatement(insert);
			//stmt2.executeUpdate();
			//connection2.commit();
			//}
		}

		/**
		 * Obtiene los datos del usuario y pedido con el pedido de mayor importe para la tienda dada
		 */
		public void getUserMaxOrder(long idTienda, long userId, long orderId, String name, String address) throws SQLException {

			// primero obtenemos el pedido con mayor importe de la tienda dada, y luego buscamos el usuario con el pedido con ese importe
			// puede haber varios usuarios con ese mismo importe maximo. Podriamos devolver solo el primero, pero vamos a devolver todos
			final String query = "SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO WHERE P.TOTAL = " +
			"(SELECT MAX(P.TOTAL) FROM PEDIDOS AS P WHERE P.ID_TIENDA = ?)";
			//String query = String.format("SELECT U.ID_USUARIO, P.ID_PEDIDO, P.TOTAL, U.NOMBRE, U.DIRECCION FROM PEDIDOS AS P INNER JOIN USUARIOS AS U ON P.ID_USUARIO = U.ID_USUARIO WHERE P.ID_TIENDA = ?", idTienda);
			final Connection connection = getConnection();
			final PreparedStatement stmt = connection.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			double total = 0;
			// cogemos solo el primero 
			//while (rs.next()) {
				if(rs.next()) {
				//if (rs.getLong("TOTAL") > total) {

					total = rs.getLong("TOTAL");
					userId = rs.getInt("ID_USUARIO");
					orderId = rs.getInt("ID_PEDIDO");
					name = rs.getString("NOMBRE");
					address = rs.getString("DIRECCION");
				}
				//}
			//}
			// NOTA: las variables que se pasan como parametros en el metodo son una copia, es decir, al modificar sus valores se modifica el de la copia y no el original
		}

		private Connection getConnection() {

			// return JDBC connection
			return null;
		}
	}

