/**
 * 
 */
package com.daretec.collection.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.daretec.collection.DoubleLinkedList;
import com.daretec.collection.exception.InvalidException;
import com.daretec.collection.impl.DoubleLinkedListImpl;

/**
 * Clase de tests unitarios para DoubleLinkedListImpl
 * 
 * @author abelon
 *
 */
public class DoubleLinkedListTest {
	
	@Test
	public void defaultList() {
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>();
		
		Assert.assertEquals(0, list.size());
	}
	
	/**
	 * Construir lista desde una coleccion
	 */
	@Test
	public void listFromCollection() {
		final List<String> collection = Arrays.asList("one", "two", "three");
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>(collection);
		
		Assert.assertEquals(3, list.size());
		Assert.assertEquals("one", list.get(0));
		Assert.assertEquals("two", list.get(1));
		Assert.assertEquals("three", list.get(2));
	}
	
	@Test
	public void addOneElementAndRemove() {
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>();
		
		list.add("one");

		Assert.assertEquals(1, list.size());
		Assert.assertEquals("one", list.get(0));
		Assert.assertTrue(list.remove("one"));
		Assert.assertEquals(0, list.size());
		Assert.assertNull(list.get(0));
	}
	
	@Test
	public void addMultipleElementsAndRemove() {
		final DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertEquals(3, list.size());
		
		Assert.assertEquals("one", list.get(0));
		Assert.assertEquals("two", list.get(1));
		Assert.assertEquals("three", list.get(2));
		
		list.add(2, "four");
		
		Assert.assertEquals("one", list.get(0));
		Assert.assertEquals("two", list.get(1));
		Assert.assertEquals("four", list.get(2));
		Assert.assertEquals("three", list.get(3));
		
		Assert.assertEquals(4, list.size());
		
		list.remove(1);
		
		Assert.assertEquals("one", list.get(0));
		Assert.assertEquals("four", list.get(1));
		Assert.assertEquals("three", list.get(2));
		
		Assert.assertEquals(3, list.size());
	}
	
	@Test
	public void containsElement() {
		final DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertTrue(list.contains("two"));
		Assert.assertTrue(!list.contains("four"));
	}
	
	@Test
	public void convertToArray() {
		final DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertArrayEquals(list.toArray(), new Object[]{"one", "two", "three"});
	}
	
	@Test
	public void containsAll() {
		List<String> collection = Arrays.asList("one", "two", "three");
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>(collection);

		Assert.assertTrue(list.containsAll(collection));
		
		collection = Arrays.asList("one", "two", "three", "four");
		
		Assert.assertTrue(!list.containsAll(collection));
	}
	
	@Test
	public void clearList() {
		final DoubleLinkedList<String> list = this.buildDoubleLinkedList();

		list.clear();
		
		Assert.assertTrue(list.isEmpty());
	}
	
	@Test
	public void indexOfFirstElement() {
		DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertEquals(0, list.indexOf("one"));
		Assert.assertEquals(1, list.indexOf("two"));
		Assert.assertEquals(2, list.indexOf("three"));
		Assert.assertEquals(-1, list.indexOf("notfound"));
		
		list = new DoubleLinkedListImpl<>();
		// aniadimos 2 elementos iguales
		list.add("one");
		list.add("two");
		list.add("two");
		
		Assert.assertEquals(1, list.indexOf("two"));
	}
	
	@Test
	public void indexOfLastElement() {
		DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertEquals(0, list.indexOf("one"));
		Assert.assertEquals(1, list.indexOf("two"));
		Assert.assertEquals(2, list.indexOf("three"));
		Assert.assertEquals(-1, list.indexOf("notfound"));
		
		list = new DoubleLinkedListImpl<>();
		// aniadimos 2 elementos iguales
		list.add("one");
		list.add("two");
		list.add("two");
		
		Assert.assertEquals(2, list.lastIndexOf("two"));
	}
	
	@Test
	public void getElement() {
		final DoubleLinkedList<String> list = this.buildDoubleLinkedList();
		
		Assert.assertEquals("three", list.get(2));
		Assert.assertNotEquals("one", list.get(1));
	}
	
	@Test(expected = InvalidException.class)
	public void invalidAddOnIndex() {
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>();
		
		list.add(2, "one");
	}
	
	/**
	 * Builder
	 * 
	 * @return double linkedlist
	 */
	private DoubleLinkedList<String> buildDoubleLinkedList() {
		final DoubleLinkedList<String> list = new DoubleLinkedListImpl<>();
		// aniadimos 3 elementos
		list.add("one");
		list.add("two");
		list.add("three");
		
		return list;
	}

}
