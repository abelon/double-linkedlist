/**
 * 
 */
package com.daretec.collection;

/**
 * Definicion de nodo que encapsula un objeto y las referencias al nodo anterior y posterior
 * 
 * @author abelon
 *
 */
public interface Node<E> {
	
	E data();
	
	Node<E> next();
	
	Node<E> prev();
	
	void data(E e);
	
	void next(Node<E> n);
	
	void prev(Node<E> n);

}
