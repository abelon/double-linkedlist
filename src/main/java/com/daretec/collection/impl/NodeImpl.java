/**
 * 
 */
package com.daretec.collection.impl;

import com.daretec.collection.Node;

/**
 * @author abelon
 *
 */
public class NodeImpl<E> implements Node<E> {
	
	private E data;
	private Node<E> next;
	private Node<E> prev;
	
	public NodeImpl(final E data, final Node<E> next, final Node<E> prev) {
		this.data = data;
		this.next = next;
		this.prev = prev;
	}

	@Override
	public E data() {
		return this.data;
	}

	@Override
	public Node<E> next() {
		return this.next;
	}

	@Override
	public Node<E> prev() {
		return this.prev;
	}

	@Override
	public void next(final Node<E> n) {
		this.next = n;
	}

	@Override
	public void prev(final Node<E> n) {
		this.prev = n;
	}

	@Override
	public void data(final E e) {
		this.data = e;
	}

}
