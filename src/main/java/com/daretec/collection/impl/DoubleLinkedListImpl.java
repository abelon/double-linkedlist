/**
 * 
 */
package com.daretec.collection.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Optional;

import com.daretec.collection.DoubleLinkedList;
import com.daretec.collection.Node;
import com.daretec.collection.exception.InvalidException;

/**
 * @author abelon
 *
 */
public class DoubleLinkedListImpl<E> implements DoubleLinkedList<E> {

	protected int size; // numero de elementos de la lista. Por defecto 0
	protected Node<E> first; // primer nodo
	protected Node<E> last; // ultimo nodo

	/**
	 * Constructor por defecto
	 */
	public DoubleLinkedListImpl() {
	}

	/**
	 * Constructor a partir de una coleccion de elementos
	 * 
	 * @param c
	 */
	public DoubleLinkedListImpl(final Collection<E> c) {
		this.addAll(c);
	}
	
	/**
	 * Construye un objeto Node con la implementacion por defecto
	 * 
	 * @param data
	 * @param next
	 * @param prev
	 * @return node
	 */
	private Node<E> builder(final E data, final Node<E> next, final Node<E> prev) {
		return new NodeImpl<E>(data, next, prev);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#size()
	 */
	@Override
	public int size() {
		return this.size;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return this.size == 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#contains(java.lang.Object)
	 */
	@Override
	public boolean contains(final Object o) {
		boolean contains = false;
		if (Objects.nonNull(o)) {
			// se recorren los nodos hasta encontrar el primer elemento
			for (Node<E> n = this.first; Objects.nonNull(n) && !contains; n = n.next()) {
				if (o.equals(n.data())) {
					contains = true;
				}
			}
		}
		return contains;
	}
	
	/* (non-Javadoc)
	 * @see java.util.List#subList(int, int)
	 */
	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public <E> E[] toArray(E[] a) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.List#toArray()
	 */
	@Override
	public Object[] toArray() {
		final Object[] array = new Object[this.size];
		Node<E> node = this.first;
		int element = 0;
		while (Objects.nonNull(node)) {
			array[element] = node.data();
			node = node.next();
			element++;
		}
		return array;
	}

	@Override
	public boolean add(final E e) {
		boolean added = false;
		if (Objects.nonNull(e)) {
			this.add(this.size, e);
			added = true;
		}
		return added;
	}

	@Override
	public boolean remove(final Object o) {
		boolean removed = false;
		if (this.size != 0 && Objects.nonNull(o)) {
			for (Node<E> n = this.first; Objects.nonNull(n) && !removed; n = n.next()) {
				if (o.equals(n.data())) {
					final Node<E> prev = n.prev();
					final Node<E> next = n.next();
					if (Objects.nonNull(prev)) {
						prev.next(next);
					}
					if (Objects.nonNull(prev)) {
						next.prev(prev);
					}
					this.size--;
					removed = true;
				}
			}
		}
		return removed;
	}

	/* (non-Javadoc)
	 * @see java.util.List#containsAll(java.util.Collection)
	 */
	@Override
	public boolean containsAll(final Collection<?> c) {
		return (Objects.nonNull(c) && !c.isEmpty()) ? c.stream().allMatch(e -> this.contains(e)) : false;
	}

	/* (non-Javadoc)
	 * @see java.util.List#addAll(java.util.Collection)
	 */
	@Override
	public boolean addAll(final Collection<? extends E> c) {
		return this.addAll(this.size, c);
	}

	/* (non-Javadoc)
	 * @see java.util.List#addAll(int, java.util.Collection)
	 */
	@Override
	public boolean addAll(final int index, final Collection<? extends E> c) {
		boolean added = false;
		if (Objects.nonNull(c) && !c.isEmpty() && 0 <= index && index <= this.size) {
			c.forEach(e -> {
				add(e);
			});
			added = true;
		}
		return added;
	}

	/* (non-Javadoc)
	 * @see java.util.List#removeAll(java.util.Collection)
	 */
	@Override
	public boolean removeAll(final Collection<?> c) {
		boolean removed = false;
		if (Objects.nonNull(c) && !c.isEmpty()) {
			c.forEach(e -> {
				remove(e);
			});
			removed = true;
		}
		return removed;
	}

	/* (non-Javadoc)
	 * @see java.util.List#retainAll(java.util.Collection)
	 */
	@Override
	public boolean retainAll(final Collection<?> c) {
		if (!this.isEmpty() && Objects.nonNull(c)) {
			final List<E> itemsToRemove = new ArrayList<>();
			for (Node<E> n = this.first; Objects.nonNull(n); n = n.next()) {
				if(!c.contains(n.data())) {
					itemsToRemove.add(n.data());
				}
			}
			
			this.removeAll(itemsToRemove);
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.util.List#clear()
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(this.size - 1);
		}
	}

	@Override
	public E get(final int index) {
		return this.getNode(index).orElse(null);
	}

	/* (non-Javadoc)
	 * @see java.util.List#set(int, java.lang.Object)
	 */
	@Override
	public E set(final int index, final E element) {
		// TODO
		return null;
	}
	
	/**
	 * Busca un nodo en la lista
	 * 
	 * @param index
	 * @return element
	 */
	private Optional<E> getNode(final int index) {
		Optional<E> result = Optional.empty();
		if (this.size != 0 && 0 <= index && index < this.size) {
			Node<E> node = this.first;
			int count = 0;
			while (count < index) {
				node = node.next();
				count++;
			}
			result = Optional.of(node.data());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.util.List#add(int, java.lang.Object)
	 */
	@Override
	public void add(final int index, final E element) {
		if (Objects.nonNull(element) && 0 <= index && index <= this.size) {
			final Node<E> newNode = this.builder(element, null, null);
			if (this.isEmpty()) { 
				// aniadir como primer elemento
				this.first = this.last = newNode;
			} else if (this.size == 1) {
				// aniadir como segundo elemento
				newNode.prev(this.first);
				this.first.next(newNode);
				this.last = newNode;
			} else if (this.size == index) {
				// aniadir como ultimo elemento
				newNode.prev(this.last);
				this.last.next(newNode);
				this.last = newNode;
			} else {
				int count = 1;
				Node<E> node = this.first;
				// recorremos hasta encontrar con el del indice
				while (count < index) {
					node = node.next();
					count++;
				}
				// modificamos referencias
				newNode.prev(node);
				newNode.next(node.next());
				node.next(newNode);
				newNode.next().prev(newNode);
			}
			this.size++;
		} else {
			throw new InvalidException("Invalid index or element null");
		}
	}

	/* (non-Javadoc)
	 * @see java.util.List#remove(int)
	 */
	@Override
	public E remove(final int index) {
		E data = null;
		if(0 <= index && index <= this.size - 1) {
			int count = 0;
			Node<E> node = this.first;
			while (count < index) {
				node = node.next();
				count++;
			}
			data = node.data();
			if(Objects.nonNull(node.prev())) {
				node.prev().next(node.next());
			}
			if(Objects.nonNull(node.next())) {
				node.next().prev(node.prev());
			}
			node.next(null);
			node.prev(null);
			this.size--;
		}
		return data;
	}

	/**
	 * Retorna la posicion del primer elemento que encuentra en la lista
	 * Si o es null o no lo encuentra retorna -1
	 */
	@Override
	public int indexOf(final Object o) {
		int index = -1;
		boolean finded = false;
		if (Objects.nonNull(o)) {
			for (Node<E> n = this.first; Objects.nonNull(n) && !finded; n = n.next()) {
				if (o.equals(n.data())){
					finded = true;
				}
				index++;
			}
		}
		return finded ? index : -1;
	}

	/**
	 * Retorna la posicion del ultimo elemento que encuentra en la lista
	 * Si o es null o no lo encuentra retorna -1
	 */
	/* (non-Javadoc)
	 * @see java.util.List#lastIndexOf(java.lang.Object)
	 */
	@Override
	public int lastIndexOf(final Object o) {
		int index = this.size;
		boolean finded = false;
		if (Objects.nonNull(o)) {
			for (Node<E> n = this.last; Objects.nonNull(n) && !finded; n = n.prev()) {
				if (o.equals(n.data())){
					finded = true;
				}
				index--;
			}
		}
		return finded ? index : -1;
	}
	
	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.List#listIterator()
	 */
	@Override
	public ListIterator<E> listIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see java.util.List#listIterator(int)
	 */
	@Override
	public ListIterator<E> listIterator(int index) {
		// TODO Auto-generated method stub
		return null;
	}

}
