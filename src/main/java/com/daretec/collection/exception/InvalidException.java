/**
 * 
 */
package com.daretec.collection.exception;

/**
 * @author abelon
 *
 */
public class InvalidException extends RuntimeException {

	private static final long serialVersionUID = 2042505816765473860L;
	
	/**
	 * Constructor
	 * 
	 * @param message
	 */
	public InvalidException(final String message) {
		super(message);
	}

}
