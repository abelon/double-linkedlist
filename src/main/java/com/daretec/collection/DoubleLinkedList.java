/**
 * 
 */
package com.daretec.collection;

import java.util.List;

/**
 * 
 * Interface listas doblemente enlazadas
 * 
 * @author abelon
 *
 */
public interface DoubleLinkedList<E> extends List<E> {

}
